import datetime
import matplotlib.pyplot as plt
from shading_calculator_for_model import shading_calculator_for_model
from shading_logger import shading_logger
import traceback

if __name__ == "__main__":

    try:
        # set up visualizer interfaces - visualizer only for debugging purposes!
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_xlabel("X axis")
        ax.set_ylabel("Y axis")

        # input files paths
        obj_file_path = r"C:\Soma\epitesz\Raumgleitner\obj_output_examples\archicad.obj"
        excel_path = "C:\Soma\epitesz\Raumgleitner\shading_database.xlsx"
        excel_sheet_name = "database"

        # export paths and names
        export_path = "C:\\Soma\\epitesz\\Raumgleitner\\test_output\\"
        export_filename = "test_output"

        # input times of investigation
        # times = [datetime.time(8, 25),
        #          datetime.time(10, 12),
        #          datetime.time(12, 25),
        #          datetime.time(14, 25),
        #          datetime.time(15, 45)]

        times = [datetime.time(8, 25),
                 datetime.time(10, 12),
                 datetime.time(12, 25)]

        new_obj = shading_calculator_for_model(obj_file_path=obj_file_path,
                                               sun_database_path=excel_path,
                                               sun_database_sheetname=excel_sheet_name,
                                               matplotlib_axes=ax,
                                               times=times,
                                               north_angle=0.,
                                               original_base_plane="xz",
                                               x_resolution=1.,
                                               y_resolution=1.,
                                               z_resolution=1.,
                                               base_plane_height=0.,
                                               base_plane_tolerance=10000.,
                                               number_of_bases=1,
                                               start_alpha=1000.,
                                               alpha_increment=1000.,
                                               alpha_timeout_interval=10.,
                                               aim_cover_percentage=.9,
                                               simplification_tolerance=10.,
                                               export_path=export_path,
                                               export_filename=export_filename,
                                               do_display=True)

    except Exception:
        st = traceback.format_exc()
        shading_logger.error("Unexpected error occured: {0}".format(st))
