import logging.config
import logging
import datetime
import os
import sys

raw_log_path = os.getcwd()
while not os.path.isdir(raw_log_path + "\\logs"):
    raw_log_path = os.path.dirname(raw_log_path)

log_path = raw_log_path.replace("\\", "/")

logging.config.fileConfig(os.path.join(os.path.dirname(__file__), "shading_logging.conf"),
                          disable_existing_loggers=False,
                          defaults={"logfilename": log_path + r"/logs/shading_{0}.log"
                          .format(datetime.datetime.now().date())})

shading_logger = logging.getLogger("root")
