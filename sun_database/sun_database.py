import datetime
from typing import List
from sun_database_importer_methods import import_from_excel, ExcelHeaders as Eh
import math


class SunDatabaseInterval:

    """
    SUN DATABASE INTERVAL
    The class defines a timeframe (start and stop) and the azimuth and altitude angles related to that timeframe in
    radians!
    """

    def __init__(self, start: datetime.time, end: datetime.time, azimuth: float, altitude: float):
        self.start = start
        self.end = end
        self.azimuth = azimuth
        self.altitude = altitude

    def in_interval(self, time: datetime.time) -> bool:
        if self.start <= time <= self.end:
            return True
        else:
            return False

    def __str__(self):
        return "Start : {0}, End: {1}, Azimuth: {2}, Altitude: {3}".format(self.start,
                                                                           self.end,
                                                                           self.azimuth,
                                                                           self.altitude)


class SunDatabase:

    """
    SUN DATABASE CLASS
    The class is a specialized list, only allowing to add SunDataBaseIntervals to itself. An instance of the class
    covers a time frame and gives back the related azimuth and altitude angles.
    """

    def __init__(self, name: str):
        self.name = name
        self.intervals = []  # type: List[SunDatabaseInterval]

    def add_interval(self, input_interval: SunDatabaseInterval):
        if isinstance(input_interval, SunDatabaseInterval):
            self.intervals.append(input_interval)
        else:
            raise TypeError("The {0} to be added is not a Sun Database Interval!".format(input_interval))

    def add_intervals(self, input_intervals: List[SunDatabaseInterval]):
        for interval in input_intervals:
            self.add_interval(interval)

    @property
    def timeframe_min(self):
        return min([iv.start for iv in self.intervals])

    @property
    def timeframe_max(self):
        return max([iv.end for iv in self.intervals])

    @property
    def timeframe(self):
        return self.timeframe_min, self.timeframe_max

    def in_timeframe(self, time: datetime.time):
        if self.timeframe_min <= time <= self.timeframe_max:
            return True
        else:
            return False

    def get_interval_of(self, time: datetime.time) -> SunDatabaseInterval:
        if self.in_timeframe(time):
            return [iv for iv in self.intervals if iv.in_interval(time)][0]
        else:
            raise ValueError("The given time {0} is outside of the timeframe ({1}, {2}) of the current database {3}"
                             .format(time,
                                     self.timeframe_min.isoformat(),
                                     self.timeframe_max.isoformat(),
                                     self.name))

    @staticmethod
    def from_excel(file_path: str, sheet_name: str):
        sun_database = SunDatabase(file_path)
        raw_listofdict = import_from_excel(file_path, sheet_name)
        for dct in raw_listofdict:
            sun_db_interval = SunDatabaseInterval(start=dct[Eh.START.value],
                                                  end=dct[Eh.END.value],
                                                  azimuth=math.radians(dct[Eh.AZIMUTH.value]),
                                                  altitude=math.radians(dct[Eh.ALTITUDE.value]))
            sun_database.add_interval(sun_db_interval)
        return sun_database

