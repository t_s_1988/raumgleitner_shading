from shapely.ops import cascaded_union, polygonize
from shapely import geometry
from scipy.spatial import Delaunay
import numpy as np
import math
import matplotlib.pyplot as plt
import shapely_operations.shapely_plotter as shpl
from typing import List, Tuple


def alpha_shape(coord_seq: List[Tuple[float, float]], alpha: float):

    """
    Compute the alpha shape (concave hull) of a set
    of points.
    :param coord_seq: Iterable container of points.
    :param alpha: alpha value to influence the
        gooeyness of the border. Smaller numbers
        don't fall inward as much as larger numbers.
        Too large, and you lose everything!
    """

    if len(coord_seq) < 4:
        # When you have a triangle, there is no sense
        # in computing an alpha shape.
        return geometry.MultiPoint(coord_seq).convex_hull

    if alpha == 0:
        # if alpha is set to 0, then the algorithm has to return the original point cloud
        return geometry.MultiPoint(coord_seq)

    coords = np.array(coord_seq)

    tri = Delaunay(coords)
    edges = set()
    edge_points = []
    # loop over triangles:
    # ia, ib, ic = indices of corner points of the
    # triangle
    for ia, ib, ic in tri.vertices:
        pa = coords[ia]
        pb = coords[ib]
        pc = coords[ic]
        # Lengths of sides of triangle
        a = math.sqrt((pa[0]-pb[0])**2 + (pa[1]-pb[1])**2)
        b = math.sqrt((pb[0]-pc[0])**2 + (pb[1]-pc[1])**2)
        c = math.sqrt((pc[0]-pa[0])**2 + (pc[1]-pa[1])**2)
        # Semi-perimeter of triangle
        # s = (a + b + c)/2.0
        # Area of triangle by Heron's formula
        # area = math.sqrt(s*(s-a)*(s-b)*(s-c))
        area = geometry.Polygon([pa, pb, pc]).area
        if area > 0.:
            circum_r = a*b*c/(4.0*area)
            # Here's the radius filter.
            # print circum_r
            # if circum_r < 1.0/alpha:
            if circum_r < alpha:
                add_edge(edges, edge_points, coords, ia, ib)
                add_edge(edges, edge_points, coords, ib, ic)
                add_edge(edges, edge_points, coords, ic, ia)
    m = geometry.MultiLineString(edge_points)
    triangles = list(polygonize(m))
    casc_union = cascaded_union(triangles)  # , edge_points
    if isinstance(casc_union, geometry.Polygon):
        return geometry.MultiPolygon([casc_union])
    else:
        return casc_union


def mls_creator(edge_points):
    pass


def add_edge(edges, edge_points, coords, i, j):

    """
    Add a line between the i-th and j-th points,
    if not in the list already
    """

    if (i, j) in edges or (j, i) in edges:
        # already added
        return
    edges.add((i, j))
    edge_points.append(coords[[i, j]])

if __name__ == "__main__":

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    p1 = geometry.Point((0, 0))
    p2 = geometry.Point((0, 1))
    p3 = geometry.Point((3, 2))
    p4 = geometry.Point((0, 6))
    p5 = geometry.Point((.5, 3))
    p6 = geometry.Point((2, 5))
    p7 = geometry.Point((1, 1))
    p8 = geometry.Point((8, 16))

    pt_lst = [p1, p2, p3, p4, p5, p6, p7]
    mp = [p.coords[0] for p in pt_lst]
    for p in pt_lst:
        shpl.shapely_plot(p, ax)
    plt.waitforbuttonpress()
    al_sh = alpha_shape(mp, 2)
    shpl.shapely_plot(al_sh, ax)
    plt.waitforbuttonpress()
