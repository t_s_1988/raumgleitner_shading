
def str_to_float(input_str : str):

    """
    The function takes a string as argument and retruns its float value - if possible. If the string value can not be
    converted into a float, then the original string is returned.
    :param input_str: a string value
    :return: a float or a string value
    """

    try:
        return float(input_str)
    except ValueError:
        return input_str


def str_to_int(input_str : str):

    """
    The function takes a string as argument and retruns its integer value - if possible. If the string value can not be
    converted into an integer, then the original string is returned.
    :param input_str: a string value
    :return: an integer or a string value
    """

    try:
        return int(input_str)
    except ValueError:
        return input_str
