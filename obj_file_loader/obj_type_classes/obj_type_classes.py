import abc
from base_type_conventers import str_to_int
from shapely.geometry import Point, Polygon


class AbsObjType(metaclass=abc.ABCMeta):
    @staticmethod
    @abc.abstractstaticmethod
    def construct(obj_file, obj_group, *args, **kwargs):
        pass

    @property
    @abc.abstractproperty
    def shapely_form(self):
        pass

    @abc.abstractmethod
    def obj_file_line(self) -> str:
        pass


class Vertex(AbsObjType):
    def __init__(self, x: float, y: float, z: float):
        self.x = x
        self.y = y
        self.z = z

    @staticmethod
    def construct(obj_file, obj_group, *args, **kwargs):
        if args[4] == "xy":
            obj_type_instance = Vertex(args[1], args[2], args[3])
        elif args[4] == "xz":
            obj_type_instance = Vertex(args[1], -args[3], args[2])
        elif args[4] == "yz":
            obj_type_instance = Vertex(-args[3], args[2], args[1])
        else:
            raise NotImplementedError("Not valid coordinate system plane: {0}".format(args[4]))
        # if obj_type_instance not in obj_file.vertices:
        # if not any([v for v in obj_file.vertices if v.x - x_prox <= obj_type_instance.x <= v.x + x_prox and
        #                                             v.y - y_prox <= obj_type_instance.y <= v.y + y_prox and
        #                                             v.z - z_prox <= obj_type_instance.z <= v.z + z_prox]):
        obj_file.vertices.append(obj_type_instance)
        if obj_group is not None:
            obj_group.vertices.append(obj_type_instance)

    @staticmethod
    def from_shapely_point(inp: Point):
        return Vertex(inp.x, inp.y, inp.z)

    @property
    def shapely_form(self) -> Point:
        return Point((self.x, self.y, self.z))

    def obj_file_line(self) -> str:
        return "v {0} {1} {2}\n".format(self.x, self.y, self.z)

    def __eq__(self, other):
        return (self.x, self.y, self.z) == (other.x, other.y, other.z)

    def __hash__(self):
        return hash((self.x, self.y, self.z))


class TextureVertex(AbsObjType):
    _obj_file_class_attribute = "texture_vertices"

    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    @staticmethod
    def construct(obj_file, obj_group, *args, **kwargs):
        obj_type_instance = TextureVertex(args[1], args[2])
        obj_file.texture_vertices.append(obj_type_instance)
        if obj_group is not None:
            obj_group.texture_vertices.append(obj_type_instance)

    @property
    def shapely_form(self) -> Point:
        return Point((self.x, self.y))

    def obj_file_line(self) -> str:
        return "vt {0} {1}\n".format(self.x, self.y)


class VertexNormal(Vertex):
    @staticmethod
    def construct(obj_file, obj_group, *args, **kwargs):
        obj_type_instance = Vertex.construct(obj_group, args)
        obj_file.vertex_normals.append(obj_type_instance)
        if obj_group is not None:
            obj_group.vertex_normals.append(obj_type_instance)

    def obj_file_line(self):
        return "vn {0} {1} {2}\n".format(self.x, self.y, self.z)


class Face(AbsObjType):
    def __init__(self):
        self.vertices = []
        self.texture_vertices = []
        self.vertex_normals = []

    @staticmethod
    def construct(obj_file, obj_group, *args, **kwargs):
        face_attributes = ["vertices", "texture_vertices", "vertex_normals"]
        face = Face()
        for ar in args[1:]:
            split_ars = ar.split("/")
            v_ars = [str_to_int(st) for st in split_ars]
            for i, v in enumerate(v_ars):
                curr_attr_face = getattr(face, face_attributes[i])
                curr_attr_obj_file = getattr(obj_file, face_attributes[i])
                # v - 1 because list indexing starts from 1 in a .obj file
                if len(curr_attr_obj_file) > 0:
                    curr_attr_face.append(curr_attr_obj_file[v - 1])
        obj_file.faces.append(face)
        if obj_group is not None:
            obj_group.faces.append(face)

    @property
    def shapely_form(self):
        return Polygon([(v.x, v.y, v.z) for v in self.vertices])

    @property
    def level(self) -> float:
        return min([v.z for v in self.vertices])

    def obj_file_line(self, obj_file):
        face_line = "f"

        for ind, v in enumerate(self.vertices):
            v_part = "{0}".format(obj_file.vertices.index(v) + 1)
            if len(self.texture_vertices) > 0:
                v_part += "/{0}".format(obj_file.texture_vertices.index(self.texture_vertices[ind]) + 1)
            if len(self.vertex_normals) > 0:
                v_part += "/{0}".format(obj_file.vertex_normals.index(self.vertex_normals[ind]) + 1)
            face_line += (" " + v_part)

        return face_line + "\n"


class Comment(AbsObjType):
    def __init__(self, message: str):
        self.message = message

    @staticmethod
    def construct(obj_file, obj_group,  *args, **kwargs):
        obj_type_instance = Comment(" ".join([str(e) for e in args[1:]]))
        obj_file.comments.append(obj_type_instance)
        if obj_group is not None:
            obj_group.comments.append(obj_type_instance)

    @property
    def shapely_form(self):
        return None

    def obj_file_line(self):
        return "# {0}".format(self.message)


class NotImplementedInTypeList(AbsObjType):
    def __init__(self, not_implemented: str):
        self.obj_type = not_implemented


    @staticmethod
    def construct(obj_file, obj_group, *args, **kwargs):
        obj_type_instance = NotImplementedInTypeList(args[0])
        obj_file._not_implemented_in_type_list.append(obj_type_instance)
        if obj_group is not None:
            obj_group._not_implemented_in_type_list.append(obj_type_instance)

    @property
    def shapely_form(self):
        return None

    def obj_file_line(self):
        return ""


class NotImplementedClass(AbsObjType):
    def __init__(self, not_implemented: str):
        self.obj_type = not_implemented

    @staticmethod
    def construct(obj_file, obj_group, *args, **kwargs):
        obj_type_instance = NotImplementedClass(args[0])
        obj_file._not_implemented_class.append(obj_type_instance)
        if obj_group is not None:
            obj_group._not_implemented_class.append(obj_type_instance)

    @property
    def shapely_form(self):
        return None

    def obj_file_line(self):
        return ""
