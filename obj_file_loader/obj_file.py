from obj_file_loader.obj_types_list import ObjTypesList
from base_type_conventers import str_to_float
import time
from typing import List
import weakref
import copy


class ObjFile:

    def __init__(self):
        self.vertices = []
        self.texture_vertices = []
        self.vertex_normals = []
        self.faces = []
        self.comments = []
        self.groups = []
        self._not_implemented_class = []
        self._not_implemented_in_type_list = []

    def load_from_file(self, file_path: str, keys_to_ignore: List[str]=None, input_base_plane: str="xy"):
        keys_to_ignore = [] if keys_to_ignore is None else keys_to_ignore
        start = time.time()
        obj_group = None
        for ind, line in enumerate(ObjFile.read_txt_gen(file_path)):
            obj_group = self.__process_obj_line(line, keys_to_ignore, input_base_plane, obj_group)
            # split_list = line.split()
            # # if the current line is an empty line, follow to the next line
            # if len(split_list) > 0 and split_list[0] not in keys_to_ignore:
            #     type_args = [str_to_float(st) for st in split_list]
            #     # check if type is a group type - initialize group - objects and groups are considered!
            #     if type_args[0] in ("g", "o"):
            #         obj_group = ObjGroup(type_args[1], self)
            #         continue
            #     # check if type is a vertex type - if yes, add the baseplane to the type init arguments
            #     if type_args[0] in ("v", "vn"):
            #         type_args.append(input_base_plane)
            #     # get class type from ObjTypesList and append to appropriate class attribute
            #     self.type_creator_factory(obj_group, type_args)
            # # print(ind)
        print("Loading from .txt file lasted for {}".format(time.time() - start))
        return self

    def load_from_one_ine_txt_str(self, txt_str: str, key_to_ignore: list, input_base_plane: str):

        """
        Reads in a .obj file that is in a string fromat, lines are separated with the "\n" symbol.
        :param txt_str: a string, with the contents of a .obj file
        :return: an ObjFile instance(self)
        """

        split_line_list = txt_str.split("\n")
        obj_group = None
        for line in split_line_list:
            obj_group = self.__process_obj_line(line, key_to_ignore, input_base_plane, obj_group)
        return self

    def __process_obj_line(self, obj_file_line: str, keys_to_ignore: list, input_base_plane: str, obj_group):

        """
        The function processes one line of a .obj file.
        :param obj_file_line: a string, coming from an obj file line
        :return: does not return anything, but fills up the object with data base on the file line.
        """

        split_list = obj_file_line.split()
        # if the current line is an empty line, follow to the next line
        if len(split_list) > 0 and split_list[0] not in keys_to_ignore:
            type_args = [str_to_float(st) for st in split_list]
            # check if type is a group type - initialize group - objects and groups are considered!
            if type_args[0] in ("g", "o"):
                obj_group = ObjGroup(type_args[1], self)
                return obj_group
            # check if type is a vertex type - if yes, add the baseplane to the type init arguments
            if type_args[0] in ("v", "vn"):
                type_args.append(input_base_plane)
            # get class type from ObjTypesList and append to appropriate class attribute
            self.type_creator_factory(obj_group, type_args)
            return obj_group

    def dump_to_str_list(self, file_name: str, txt_list=None, call_level: int=0, cp_obj=None):

        if txt_list is None:
            txt_list = list()
            txt_list.append("# FILE {0}\n".format(file_name))

        if any(self.groups):
            for g in self.groups:
                txt_list.append(g.obj_file_line)
                txt_list.append("\n")
                g.dump_to_str_list(file_name, txt_list, cp_obj)
                txt_list.append("\n")

        else:

            if any(self.vertices):
                for v in self.vertices:
                    ln = v.obj_file_line()
                    txt_list.append(ln)
                txt_list.append("# Vertex indices from {0} to {1}\n".format(0, len(self.vertices)))

            if any(self.texture_vertices):
                for vt in self.texture_vertices:
                    ln = vt.obj_file_line()
                    txt_list.append(ln)
                txt_list.append("# Texture vertex indices from {0} to {1}\n".format(0, len(self.texture_vertices)))

            if any(self.vertex_normals):
                for vn in self.vertex_normals:
                    ln = vn.obj_file_line()
                    txt_list.append(ln)
                txt_list.append("# Vertex normal indices from {0} to {1}\n".format(0, len(self.vertex_normals)))

            if any(self.faces):
                for fa in self.faces:
                    if isinstance(self, ObjGroup):
                        txt_list.append(fa.obj_file_line(self.host_obj_file()))
                    else:
                        txt_list.append(fa.obj_file_line(self))
                txt_list.append("# Face indices from {0} to {1}\n".format(0, len(self.faces)))

        if call_level == 0:
            output_str = ""
            for ln in txt_list:
                ln = ln if ln[-1] != "\n" else ln + "\n"
                output_str += ln
            return output_str

    def dump_to_file(self, file_path: str, file_name: str, txt_list=None, call_level: int=0, cp_obj=None):

        txt_str = self.dump_to_str_list(file_name)

        with open(file_path + file_name, "w", encoding="utf-8") as f:
            f.write(txt_str)
            f.close()

    def type_creator_factory(self, obj_group, type_args, *args, **kwargs):
        type_key = type_args[0]
        ObjTypesList.get_class_for(type_key).construct(self, obj_group, *type_args, *args, **kwargs)

    @property
    def obj_levels(self):
        return sorted(set([v.z for v in self.vertices]))

    @staticmethod
    def read_txt_gen(path: str):
        with open(path, "r", encoding="utf-8") as infile:
            for line in infile:
                yield line


class ObjGroup(ObjFile):

    def __init__(self, name: str, host_obj_file: ObjFile):
        super().__init__()
        self.name = name
        self.host_obj_file = weakref.ref(host_obj_file)
        host_obj_file.groups.append(self)

    @property
    def obj_file_line(self):
        return "g {0}".format(self.name)

if __name__ == "__main__":
    obj_file = ObjFile().load_from_file(r"C:\Soma\epitesz\Raumgleitner\obj_output_examples\archicad.obj")
    obj_file.dump_to_file("C:\Soma\epitesz\Raumgleitner\obj_output_examples\\", "soma_shit.obj")
