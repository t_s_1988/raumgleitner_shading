from enum import Enum
from obj_file_loader import obj_type_classes as otc


__author__ = "Tamas Somsak"


class ObjTypesList(Enum):

    """
    OBJ TYPES CLASS
    The class links together the type which can be found in a .obj file (as first element on every line) with the
    python classes of the obj_type_classes package.
    """

    v = otc.Vertex
    vt = otc.TextureVertex
    vn = otc.VertexNormal
    f = otc.Face

    # not implemented classes in python yet

    # response class name in case the .obj type key is not implemented in the ObjTypesList class
    not_implemented_in_type_list = otc.NotImplementedInTypeList

    # treated as a special case as "#" can n ot be used as enum key
    comment = otc.Comment

    @staticmethod
    def get_class_for(obj_type_key: str):
        if obj_type_key == "#":
            return ObjTypesList.comment.value
        else:
            try:
                return getattr(ObjTypesList, obj_type_key).value
            except AttributeError:
                return ObjTypesList.not_implemented_in_type_list.value


if __name__ == "__main__":
    gh = ObjTypesList.get_class_for("v")(0, 0, 0)
    print(gh.x, gh.y, gh.z)