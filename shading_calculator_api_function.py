from sun_database import SunDatabase
from obj_file_loader import ObjFile
import datetime
import matplotlib.pyplot as plt
from shading_calculator_functions.shading_polygon_calculators import adjacent_overlapping_shading_polygon, \
    find_obj_base_polygons, shadow_polygons_at_multiple_times
from shapely_operations.shapely_plotter import shapely_plot
from shapely_operations.add_shapely_to_obj_file import add_shapely_to_obj_file
from shading_calculator_functions.vertex_operations import simplify_vertices
from matplotlib.pyplot import Axes
import time
from typing import List
from shading_logger import shading_logger


def shading_calculator_for_api(obj_file: ObjFile, sun_database: SunDatabase,
                               times: List[datetime.time],
                               north_angle: float,
                               x_resolution: float, y_resolution: float, z_resolution: float,
                               base_plane_height: float,
                               base_plane_tolerance: float,
                               number_of_bases: int,
                               start_alpha: float,
                               alpha_increment: float,
                               aim_cover_percentage: float,
                               simplification_tolerance: float,
                               export_filename: str):

    shading_logger.debug("\tOriginal vertex number: {0}".format(len(obj_file.vertices)))
    tm = time.time()

    simpl_vertices = list(simplify_vertices([v for v in obj_file.vertices if v.z >= base_plane_height],
                                            ["x", "y", "z"],
                                            x_resolution, y_resolution, z_resolution))

    shading_logger.info("\tSimplification of vertex cloud done in: {0}"
                        " Number of vertices: {1}".format(time.time() - tm, len(simpl_vertices)))

    # create base polygon of obj
    base_polys = find_obj_base_polygons(simpl_vertices, base_plane_height,
                                        base_plane_tolerance=base_plane_tolerance,
                                        start_alpha=start_alpha,
                                        alpha_increment=alpha_increment,
                                        aim_cover_percentage=aim_cover_percentage,
                                        simplification_tolerance=simplification_tolerance,
                                        num_bases=number_of_bases)

    shading_logger.info("\tFinding base polygon for model is done.")

    # create shadow polygons at desired times
    shadow_polys = shadow_polygons_at_multiple_times(times, simpl_vertices, base_polys, sun_database,
                                                     north_angle=north_angle,
                                                     base_plane_height=base_plane_height,
                                                     start_alpha=start_alpha,
                                                     alpha_increment=alpha_increment,
                                                     aim_cover_percentage=aim_cover_percentage,
                                                     simplification_tolerance=simplification_tolerance)

    shading_logger.info("\tCalculation of shading polygons is done.")

    # create merged polygons on adjacent times
    ov_poly = adjacent_overlapping_shading_polygon(shadow_polys, base_polys)

    shading_logger.info("\tCalculation of intersection areas of shading polygons is done.")

    # add merged polygons to the original ObjFile instance

    # dump ObjFile class instance to file
    new_obj = ObjFile()
    add_shapely_to_obj_file(base_polys, new_obj, as_group=True)
    add_shapely_to_obj_file(shadow_polys, new_obj, as_group=True)
    add_shapely_to_obj_file(ov_poly, new_obj, as_group=True)
    obj_txt_str_lst = new_obj.dump_to_str_list(export_filename + ".obj")

    shading_logger.info("END OF SHADING CALCULATION")

    return obj_txt_str_lst
