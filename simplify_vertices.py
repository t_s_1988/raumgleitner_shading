from typing import List
from obj_file_loader.obj_type_classes import Vertex



def simplify_vertices(vertices: List[Vertex], x_prox: float, y_prox: float, z_prox: float):
    levels = list({v.z for v in vertices})
    levels.sort()
    lvl_0 = [v for v in vertices if v.z == levels[0]]
    yield from simplify_vertices_2d(lvl_0, x_prox, y_prox, z_prox)
    ind = 1
    prev_lvl = levels[0]
    while ind < len(levels):
        curr_lvl = levels[ind]
        if curr_lvl - prev_lvl < z_prox:
            ind += 1
        else:
            curr_lvl_vtxs = [v for v in vertices if v.z == curr_lvl]
            yield from simplify_vertices_2d(curr_lvl_vtxs, x_prox, y_prox, z_prox)
            prev_lvl = curr_lvl
            ind += 1

    # yield vertices[0]
    # for ind in range(len(vertices[:-1])):
    #     v1 = vertices[ind]
    #     v2 = vertices[ind + 1]
    # # for v1, v2 in it.combinations(vertices, 2):
    #     if not is_close(v1, v2, x_prox, y_prox, z_prox):
    #         yield v2

def simplify_vertices_2d(vertices: List[Vertex], x_prox: float, y_prox: float, z_prox: float):
    yield vertices[0]
    for ind in range(len(vertices[:-1])):
        v1 = vertices[ind]
        v2 = vertices[ind + 1]
    # for v1, v2 in it.combinations(vertices, 2):
        if not is_in_proximity(v1, v2, x_prox, y_prox, z_prox):
            yield v2

def is_in_proximity(v1: Vertex, v2: Vertex, x_prox: float, y_prox: float, z_prox: float):
    if v1.x - x_prox <= v2.x <= v1.x + x_prox and v1.y - y_prox <= v2.y <= v1.y + y_prox and \
                                    v1.z - z_prox <= v2.z <= v1.z + z_prox:
        return True
    else:
        return False
