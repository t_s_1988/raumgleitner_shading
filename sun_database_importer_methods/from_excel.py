import xlrd
from sun_database_importer_methods.excel_header_enum import ExcelHeaders as Eh
from typing import List
import datetime


def import_from_excel(file_path: str, sheet_name: str, header_row_num: int=0) -> List[dict]:
    xl_wb = xlrd.open_workbook(file_path)
    xl_sheet = xl_wb.sheet_by_name(sheet_name)
    # return [xlrd.xldate_as_tuple(xl_sheet.cell(1, 1).value, xl_wb.datemode), xlrd.cellname(1, 1)]
    ret_listofdict = []
    for rw in range(header_row_num + 1, xl_sheet.nrows):
        curr_dct = {}
        for cl in range(xl_sheet.ncols):
            curr_header = xl_sheet.cell_value(0, cl)
            if curr_header in (Eh.START.value, Eh.END.value):
                tuple_time = xlrd.xldate_as_tuple(xl_sheet.cell_value(rw, cl), xl_wb.datemode)
                # datetime concersion - assuming that the time is given in hh:mm format in the excel workbook!
                tm = datetime.time(tuple_time[3], tuple_time[4])
                curr_dct[curr_header] = tm
            elif curr_header in (Eh.AZIMUTH.value, Eh.ALTITUDE.value):
                curr_dct[curr_header] = xl_sheet.cell_value(rw, cl)
            else:
                raise NotImplementedError()
        ret_listofdict.append(curr_dct)
    return ret_listofdict


if __name__ == "__main__":
    print(import_from_excel("C:\Soma\epitesz\Raumgleitner\shading_database.xlsx", "database"))
