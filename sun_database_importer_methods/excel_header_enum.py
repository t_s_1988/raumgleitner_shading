from enum import Enum


class ExcelHeaders(Enum):

    """
    EXCEL HEADER CLASS
    This class specifies how are the column headers referenced in the python code when reading a sun database from an
    excel file.
    """

    START = "Start"
    END = "End"
    AZIMUTH = "Azimuth"
    ALTITUDE = "Altitude"
