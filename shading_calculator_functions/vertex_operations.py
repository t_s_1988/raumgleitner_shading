from obj_file_loader.obj_type_classes import Vertex
from obj_file_loader.obj_file import ObjFile
from sun_database import SunDatabase
import datetime
import math
from shapely.geometry import MultiPoint, MultiPolygon
from typing import List
import matplotlib.pyplot as plt
from alpha_shape_calculator import alpha_shape
from shading_logger import shading_logger
import time


def create_vertex_shadow(input_vt: Vertex, sh_time: datetime.time, sun_database: SunDatabase,
                         shadow_plane_height: float = 0., north_angle: float = 0):
    """
    The function creates the shadow of a Vertex and gives it back as a new Vertex instance
    :param input_vt: a Vertex instance
    :param sh_time: a specified time in a datetime format
    :param sun_database: a SunDatabase instance
    :param shadow_plane_height: the height of the plane where the shadow is cast, given with a float number
    :param north_angle: the angle of the north direction in comparison to the y axis (x = 0 function line) in degrees.
    :return: a Vertex instance
    """

    north_angle = math.radians(north_angle)
    sun_interval = sun_database.get_interval_of(sh_time)
    height = input_vt.z - shadow_plane_height
    shadow_length = height * (1 / math.tan(sun_interval.altitude))
    vertex_x = input_vt.x + shadow_length * math.sin(sun_interval.azimuth + north_angle)
    vertex_y = input_vt.y + shadow_length * math.cos(sun_interval.azimuth + north_angle)
    vertex_z = shadow_plane_height

    return Vertex(vertex_x, vertex_y, vertex_z)


def find_shadow_vertices(input_vertices: List[Vertex], base_plane_height: float = 0.,
                         alpha: float = 1) -> List[Vertex]:
    return_vertices = []
    levels = list({v.z for v in input_vertices})
    levels.sort(reverse=True)
    for ind in range(len(levels)):
        curr_shadow_vertices = [v for v in input_vertices if v.z == levels[ind]]

        if len(curr_shadow_vertices) > 1:
            # get convex hull of vertices with Shapely
            hull = alpha_shape_for_vertices(curr_shadow_vertices, .6, 10., 100, 1)
            curr_shadow_vertices = [Vertex(c[0], c[1], levels[ind]) for c in hull.boundary.coords]
            return_vertices.extend(curr_shadow_vertices)

    return return_vertices


def find_base_vertices(input_obj_file: ObjFile, base_plane_height: float = 0.) -> List[Vertex]:
    """
    The function returns the convex hull of points that are on the base plane of the obj file instance
    :param input_obj_file: an ObjFile instance
    :param base_plane_height: the height of the plane where the shadow is cast, given with a float number
    :return: a list of Vertex instances
    """

    base_plane_vertices = [v for v in input_obj_file.vertices if v.z == base_plane_height]
    shapely_vertices = MultiPoint([v.shapely_form for v in base_plane_vertices])
    convex_hull = shapely_vertices.convex_hull
    return [Vertex(c[0], c[1], c[2]) for c in convex_hull.boundary.coords]


def find_base_vertices_in_tolerance(input_vertices: List[Vertex], base_plane_height: float = 0.,
                                    tolerance: float = 0.):
    """
    The function returns the convex hull of points that are on the base plane of the obj file instance
    :param input_vertices: a list of Vertex instances
    :param base_plane_height: the height of the plane where the shadow is cast, given with a float number
    :param tolerance: a float number indicating the tolerance range around the base plane, where Vertices shall be
    considered as they are in the base plane
    :return: a list of Vertex instances
    """

    max_tol = base_plane_height + tolerance
    base_plane_vertices = [v for v in input_vertices if base_plane_height <= v.z <= max_tol]
    return base_plane_vertices


def simplify_vertices(input_vertices: List[Vertex], comparison_order: list,
                      x_prox: float, y_prox: float, z_prox: float,
                      curr_cp_index: int = 0):
    if all(crd in comparison_order for crd in ["x", "y", "z"]):

        curr_comp_axis = comparison_order[curr_cp_index]
        crds_comparison = list({getattr(v, curr_comp_axis) for v in input_vertices})
        crds_comparison.sort()
        prev_crd = crds_comparison[0]
        for ind in range(len(crds_comparison)):
            curr_crd_comparison = crds_comparison[ind]
            if curr_crd_comparison - prev_crd <= eval("{0}_prox".format(curr_comp_axis)) \
                    and ind != 0:
                continue
            else:
                curr_crd_vtxs = [v for v in input_vertices
                                 if getattr(v, curr_comp_axis) == curr_crd_comparison]

                # pt = plt.scatter([v.x for v in curr_crd_vtxs], [v.y for v in curr_crd_vtxs], color="y", s=.5)
                # plt.pause(interval=.005)
                # pt.remove()

                if len(comparison_order) - 1 == curr_cp_index:

                    # plt.scatter(curr_crd_vtxs[0].x, curr_crd_vtxs[0].y, color="g", s=.5)
                    # plt.pause(interval=.005)

                    yield curr_crd_vtxs[0]
                else:
                    yield from simplify_vertices(curr_crd_vtxs, comparison_order, x_prox, y_prox, z_prox,
                                                 curr_cp_index + 1)
                prev_crd = curr_crd_comparison

    else:
        raise TypeError()


def alpha_shape_for_vertices(input_vertices: List[Vertex],
                             aim_cover_percentage: float,
                             alpha_increment: float,
                             start_alpha: float,
                             timeout_interval: float,
                             simplification_tolerance: float = 0., num_aim_polys: int = 1):
    if len(input_vertices) > 0:

        # set up starting conditions for alpha shape iteration
        vertice_coord_seq = [(v.x, v.y) for v in input_vertices]
        original_vertices_len = len(input_vertices)
        curr_alpha_shape = MultiPolygon()
        cover_percentage = 0
        timed_out = False
        timeout_start_point = time.time()

        while cover_percentage < aim_cover_percentage and not timed_out:

            last_cover_percentage = cover_percentage
            last_alpha_shape = curr_alpha_shape
            curr_alpha_shape = alpha_shape(vertice_coord_seq, start_alpha)
            num_curr_alpha_polys = len(list(curr_alpha_shape.geoms))
            intersected_vertices = []

            if num_aim_polys == num_curr_alpha_polys:
                for pl in curr_alpha_shape.geoms:
                    # plt.scatter([v.x for v in input_vertices], [v.y for v in input_vertices], s=.5)
                    # plt.plot([c[0] for c in pl.exterior.coords], [c[1] for c in pl.exterior.coords])
                    # plt.pause(.1)

                    intersected_vertices.extend([v for v in input_vertices if v.shapely_form.within(pl)])
                cover_percentage = len(intersected_vertices) / original_vertices_len

            # exit point - if number of polygons become lower than the desired limit
            if num_aim_polys > num_curr_alpha_polys and not curr_alpha_shape.is_empty:
                shading_logger.debug("The number of polyogons went lower than the desired ({0})."
                                     "Final cover percentage is {1}".format(num_aim_polys, cover_percentage))
                last_alpha_shape = curr_alpha_shape if last_alpha_shape.is_empty else last_alpha_shape
                simplified_mp = MultiPolygon([pl.simplify(simplification_tolerance) for pl in last_alpha_shape.geoms])
                return simplified_mp

            # examine if the iteration has timed out - if cover percentage does not increase any more
            # have not found proper number of base polygons yet
            if cover_percentage - last_cover_percentage > 0 or num_aim_polys != num_curr_alpha_polys:
                timeout_start_point = time.time()
            else:
                time_interval_passed = time.time() - timeout_start_point
                timed_out = time_interval_passed >= timeout_interval
                if timed_out:
                    shading_logger.debug("The cover percentage of the alpha shape have not increased in the last {0}"
                                         "seconds (set timeout interval). The algorithm finishes, highest cover "
                                         "percentage achieved: {1}".format(timeout_interval, cover_percentage))

            start_alpha += alpha_increment

        simplified_mp = MultiPolygon([pl.simplify(simplification_tolerance) for pl in curr_alpha_shape.geoms])

        return simplified_mp

    else:
        raise ValueError("The receieved Vertex list is an empty list!")

        # shapely_vertices = MultiPoint([v.shapely_form for v in base_plane_vertices])
        # plt.scatter([p.x for p in shapely_vertices], [p.y for p in shapely_vertices])
        # plt.waitforbuttonpress()
        # convex_hull = shapely_vertices.convex_hull
