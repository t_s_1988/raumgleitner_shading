import datetime
from sun_database import SunDatabase
from shading_calculator_functions.vertex_operations import create_vertex_shadow, \
    find_base_vertices_in_tolerance, alpha_shape_for_vertices
from shapely.geometry import Polygon, MultiPolygon
from typing import List
from obj_file_loader.obj_type_classes import Vertex
import matplotlib.pyplot as plt


def find_obj_base_polygons(input_vertices: List[Vertex], base_plane_height: float=0,
                           base_plane_tolerance: float=None,
                           start_alpha: float=1,
                           alpha_increment: float=10.,
                           alpha_timeout_interval: float=10.,
                           aim_cover_percentage: float=0.8,
                           simplification_tolerance: float=0.,
                           num_bases: int=1) -> MultiPolygon:

    obj_base_vertices = find_base_vertices_in_tolerance(input_vertices,
                                                        base_plane_height=base_plane_height,
                                                        tolerance=base_plane_tolerance) if base_plane_tolerance is not \
                                                                                           None else input_vertices

    hull = alpha_shape_for_vertices(input_vertices=obj_base_vertices,
                                    aim_cover_percentage=aim_cover_percentage,
                                    start_alpha=start_alpha,
                                    alpha_increment=alpha_increment,
                                    simplification_tolerance=simplification_tolerance,
                                    num_aim_polys=num_bases,
                                    timeout_interval=alpha_timeout_interval)
    # obj_base_shapely_points = [v.shapely_form for v in obj_base_vertices]
    # return Polygon(MultiPoint(obj_base_shapely_points)).simplify(0)
    hull = MultiPolygon([Polygon([(c[0], c[1], base_plane_height) for c in pl.exterior.coords]) for pl in hull])
    return hull


def shadow_polygon_at_time(time: datetime.time, shading_vertices, sun_database: SunDatabase, base_polygon: Polygon,
                           north_angle: float = 0, base_plane_height: float = 0,
                           start_alpha: float=1, alpha_increment: float=10.,
                           alpha_timeout_interval: float=10.,
                           aim_cover_percentage: float=.8,
                           simplification_tolerance: float=0.) -> MultiPolygon:

    # projection_base_vertices = find_shadow_vertices(obj_file, base_plane_height)
    shading_vertices = [v for v in shading_vertices if v.z >= base_plane_height and
                        v.shapely_form.intersects(base_polygon)]

    projected_shadow_vertices = [create_vertex_shadow(v, time, sun_database, base_plane_height, north_angle)
                                 for v in shading_vertices]

    # exclude shading vertices that are inside the base polygon
    # projected_shadow_vertices = [v for v in projected_shadow_vertices if not base_polygon.intersects(v.shapely_form)]

    # plt.scatter([v.x for v in projected_shadow_vertices], [v.y for v in projected_shadow_vertices], s=.5, color="y")
    # plt.pause(interval=.1)

    hull = alpha_shape_for_vertices(input_vertices=projected_shadow_vertices,
                                    aim_cover_percentage=aim_cover_percentage,
                                    start_alpha=start_alpha,
                                    alpha_increment=alpha_increment,
                                    simplification_tolerance=simplification_tolerance,
                                    num_aim_polys=1,
                                    timeout_interval=alpha_timeout_interval)

    hull = MultiPolygon([Polygon([(c[0], c[1], base_plane_height) for c in pl.exterior.coords]) for pl in hull])

    # for pl in hull.geoms:
    #     plt.plot([c[0] for c in pl.exterior.coords],
    #              [c[1] for c in pl.exterior.coords],
    #              color="y")
    #     plt.pause(interval=1.)

    hull = MultiPolygon([h.difference(base_polygon) for h in hull.geoms])

    return hull


def shadow_polygons_at_multiple_times(times: List[datetime.time], input_vertices: List[Vertex],
                                      base_polys: MultiPolygon, sun_database: SunDatabase,
                                      north_angle: float = 0, base_plane_height: float = 0,
                                      start_alpha: float=1000., alpha_increment: float=10.,
                                      alpha_timeout_interval: float=10.,
                                      aim_cover_percentage: float=.8, simplification_tolerance: float=0.) -> MultiPolygon:
    shadow_polys = []
    for tm in times:

        for bs in base_polys.geoms:
            sh_ply = shadow_polygon_at_time(time=tm,
                                            shading_vertices=input_vertices,
                                            sun_database=sun_database,
                                            base_polygon=bs,
                                            north_angle=north_angle,
                                            base_plane_height=base_plane_height,
                                            start_alpha=start_alpha,
                                            alpha_increment=alpha_increment,
                                            alpha_timeout_interval=alpha_timeout_interval,
                                            aim_cover_percentage=aim_cover_percentage,
                                            simplification_tolerance=simplification_tolerance)

            shadow_polys.extend([p for p in sh_ply.geoms])

    shadow_polys = MultiPolygon(shadow_polys)

    return shadow_polys


def adjacent_overlapping_shading_polygon(input_shading_mp: MultiPolygon, base_polygons: MultiPolygon) -> Polygon:

    # if only one polygon is provided - there is no overlapping, the original polygon shall be returned
    if len(input_shading_mp.geoms) == 1:
        return input_shading_mp.geoms[0]

    two_hrs_polys = []
    for ind in range(len(input_shading_mp.geoms) - 1):
        pl1 = input_shading_mp[ind]
        pl2 = input_shading_mp[ind + 1]
        merged = pl1.intersection(pl2)
        two_hrs_polys.append(merged)

    # if only one intersection polygon is created - then the overall polygon is the intersection polygon
    if len(two_hrs_polys) == 1:
        ov_poly = two_hrs_polys[0]
    else:
        ov_poly = Polygon()
        for ind in range(len(two_hrs_polys) - 1):
            dual_merged = two_hrs_polys[ind].union(two_hrs_polys[ind + 1]).convex_hull
            ov_poly = dual_merged if ov_poly.is_empty else ov_poly.union(dual_merged)

        for b_pl in base_polygons.geoms:
            ov_poly = ov_poly.difference(b_pl)

    return ov_poly
