from flask import Flask, request, Response, render_template, url_for, redirect, send_from_directory, send_file,\
    after_this_request, flash
import traceback
from shading_logger import shading_logger
from obj_file_loader import ObjFile
from sun_database import SunDatabase
from shading_calculator_api_function import shading_calculator_for_api
from shading_calculator_for_model import shading_calculator_for_model
from werkzeug.utils import secure_filename
import matplotlib.pyplot as plt
import datetime
import os
import zipfile
from shading_api.input_forms import ShadingInputForm
import time

app = Flask(__name__)
UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), "tmp\\")
ALLOWED_EXTENSIONS = {'obj', 'xls', 'xlsx'}
app.config['SECRET_KEY'] = r'\xe0\xfawts?\x1f\x9b\x10\xb7H\xa0\x0e\x01\xc6\xc9\xbd\xde\xdf\x8asr\xab&'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.logger.getChild("shading_logger.shading_logger")

excel_path = "C:\Soma\epitesz\Raumgleitner\shading_database.xlsx"
excel_sheet_name = "database"
obj_input_file_path = r"C:\Soma\epitesz\Raumgleitner\obj_output_examples\test_Schatten.obj"

times = [datetime.time(8, 25),
         datetime.time(10, 12),
         datetime.time(12, 25),
         datetime.time(14, 25),
         datetime.time(15, 45)]


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/index', methods=['GET'])
def index():
    api_response = ""
    form = ShadingInputForm()
    return render_template("index.html", api_response=api_response, input_form=form)


@app.route('/submit_project', methods=['GET', 'POST'])
def submit_project():
    try:
        # form that contains required calculation parameters for shading_calculator_for_model
        input_form = ShadingInputForm()
        # form that contains required file output formats for shading_calculator_for_model
        if input_form.validate_on_submit():
            # upload necessary files
            filename_obj = secure_filename(input_form.input_obj_file.data.filename)
            filename_sd = secure_filename(input_form.input_sd_database.data.filename)
            if allowed_file(filename_obj) and allowed_file(filename_sd):
                file_path_obj = os.path.join(app.config['UPLOAD_FOLDER'], filename_obj)
                file_path_sd = os.path.join(app.config['UPLOAD_FOLDER'], filename_sd)
                input_form.input_obj_file.data.save(file_path_obj)
                input_form.input_sd_database.data.save(file_path_sd)

            else:
                return Response(status=400)

            # create datetime from input
            input_times = []
            tm_ind = 1
            while hasattr(input_form, "time_{0}".format(tm_ind)):
                curr_input_tm = getattr(input_form, "time_{0}".format(tm_ind))
                if curr_input_tm.data is not None:
                    h_m_tm = datetime.time(curr_input_tm.data.hour, curr_input_tm.data.minute)
                    input_times.append(h_m_tm)
                tm_ind += 1

            # TODO: move matplotlib setup to separate function
            fig = plt.figure()
            ax = fig.add_subplot(111)
            export_filename = "{0}_with_shadows".format(filename_obj.split(".")[0])

            # start shading calculator function
            new_obj = shading_calculator_for_model(obj_file_path=file_path_obj,
                                                   sun_database_path=file_path_sd,
                                                   sun_database_sheetname=input_form.input_sd_sheet.data,
                                                   matplotlib_axes=ax,
                                                   times=input_times,
                                                   north_angle=float(input_form.north_direction_angle.data),
                                                   original_base_plane=input_form.base_plane.data['base_plane'],
                                                   x_resolution=float(input_form.working_resolution.data),
                                                   y_resolution=float(input_form.working_resolution.data),
                                                   z_resolution=float(input_form.working_resolution.data),
                                                   base_plane_height=float(input_form.base_plane_height.data),
                                                   base_plane_tolerance=float(input_form.base_plane_tolerance.data),
                                                   number_of_bases=1,
                                                   start_alpha=float(input_form.alpha_start.data),
                                                   alpha_increment=float(input_form.alpha_increment.data),
                                                   alpha_timeout_interval=10.,
                                                   aim_cover_percentage=float(input_form.target_cover_percentage.data),
                                                   simplification_tolerance=float(input_form.smoothening_tolerance.data),
                                                   export_path=app.config['UPLOAD_FOLDER'],
                                                   export_filename=export_filename,
                                                   do_display=False)

            # deleting original files from tmp folder
            os.remove(file_path_obj)
            os.remove(file_path_sd)
            plt.close()

            # download files that are ready
            obj_filename = export_filename + ".obj"
            pdf_filename = export_filename + ".pdf"
            if input_form.is_pdf.data and input_form.is_obj.data:
                zip_filename = export_filename + ".zip"
                zipf = zipfile.ZipFile(app.config['UPLOAD_FOLDER'] + zip_filename, "w", zipfile.ZIP_DEFLATED)
                zipf.write(app.config['UPLOAD_FOLDER'] + obj_filename, arcname=obj_filename)
                zipf.write(app.config['UPLOAD_FOLDER'] + pdf_filename, arcname=pdf_filename)
                zipf.close()
                return send_file(app.config['UPLOAD_FOLDER'] + zip_filename,
                                 mimetype='zip',
                                 attachment_filename=zip_filename,
                                 as_attachment=True)
            if input_form.is_obj.data:
                return send_from_directory(directory=os.path.abspath(app.config['UPLOAD_FOLDER']),
                                           filename=export_filename + ".obj",
                                           as_attachment=True)
            if input_form.is_pdf.data:
                return send_from_directory(directory=os.path.abspath(app.config['UPLOAD_FOLDER']),
                                           filename=export_filename + ".pdf",
                                           as_attachment=True)

        flash("Invalid input data detected")
        return redirect(url_for('index'))
    except Exception:
        st = traceback.format_exc()
        shading_logger.error("Unexpected error occured: {0}".format(st))
        return Response(status=500)


# TODO: find a suitable method to delete files after download
@app.route('/download/<filename>', methods=['GET'])
def download_file(filename):
    file_path = os.path.join(os.path.abspath(app.config['UPLOAD_FOLDER']), filename)

    @after_this_request
    def remove(response):
        can_not_remove = True
        while can_not_remove:
            try:
                os.remove(file_path)
                can_not_remove = False
            except Exception:
                print("Sleeping")
                time.sleep(2)
        return response
    return send_from_directory(directory=os.path.abspath(app.config['UPLOAD_FOLDER']), filename=filename,
                               as_attachment=True)


@app.route('/cleanup_tmp', methods=['GET'])
def cleanup():
    for file in os.listdir(app.config['UPLOAD_FOLDER']):
        curr_path = os.path.join(app.config['UPLOAD_FOLDER'], file)
        os.remove(curr_path)
    return redirect('index')


@app.errorhandler(500)
def internal_server_error(e):
    return render_template("500.html"), 500


@app.errorhandler(404)
def internal_server_error(e):
    return render_template("404.html"), 404


@app.route('/shading_for_obj', methods=['POST'])
def calculate_shading_api():
    try:
        # inp = request.data.decode('utf-8')
        obj_file = ObjFile().load_from_file(obj_input_file_path, ["vn", "f", "vt", "#"],
                                            input_base_plane=request.form['base_plane'])
        sd = SunDatabase.from_excel(excel_path, excel_sheet_name)
        output_str = shading_calculator_for_api(obj_file,
                                                sd, times,
                                                north_angle=float(request.form['north_angle']),
                                                x_resolution=float(request.form['working_resolution']),
                                                y_resolution=float(request.form['working_resolution']),
                                                z_resolution=float(request.form['working_resolution']),
                                                base_plane_height=float(request.form['base_plane_height']),
                                                base_plane_tolerance=float(request.form['base_plane_tolerance']),
                                                number_of_bases=1,
                                                start_alpha=float(request.form['alpha_start']),
                                                alpha_increment=float(request.form['alpha_increment']),
                                                aim_cover_percentage=float(request.form['target_cover_percentage']),
                                                simplification_tolerance=float(request.form['smoothening_tolerance']),
                                                export_filename="test_api_output.obj")

        return render_template("index.html", api_response=output_str)
    except Exception:
        st = traceback.format_exc()
        shading_logger.error("Unexpected error occured: {0}".format(st))
        return Response("Done", status=500)


if __name__ == "__main__":
    app.run(debug=True)
