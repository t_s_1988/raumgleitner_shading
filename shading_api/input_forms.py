from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms.fields import StringField, FloatField, SelectField, FormField, BooleanField
from wtforms.fields.html5 import DecimalField, DateTimeField
import wtforms.validators as w_val
import datetime


class BasePlaneInputForm(FlaskForm):
    base_plane = SelectField('',
                             validators=[w_val.DataRequired()],
                             choices=[("xy", "XY plane"), ("xz", "XZ plane"), ("yz", "YZ plane")],
                             default=("xy", "XY plane"))


class ShadingInputForm(FlaskForm):
    input_obj_file = FileField('input_obj_file', validators=[w_val.DataRequired()])
    input_sd_database = FileField('input_sd_database', validators=[w_val.DataRequired()])
    input_sd_sheet = StringField('input_sd_sheet', default="database", validators=[w_val.DataRequired()])
    base_plane = FormField(BasePlaneInputForm)
    north_direction_angle = DecimalField('north_angle', validators=[w_val.NumberRange(0, 359)], default=0.)
    working_resolution = DecimalField('working_resolution', validators=[], default=1)
    base_plane_height = DecimalField('base_plane_height', validators=[], default=0)
    base_plane_tolerance = DecimalField('base_plane_tolerance', validators=[], default=100)
    alpha_start = DecimalField('alpha_start', validators=[], default=100)
    alpha_increment = DecimalField('alpha_increment', validators=[], default=10)
    target_cover_percentage = DecimalField('target_cover_percentage', validators=[w_val.NumberRange(0, 1)], default=0.8)
    smoothening_tolerance = DecimalField('smoothening_tolerance', validators=[], default=1)

    # input fields for file output format
    is_obj = BooleanField('is_obj', default='y', validators=[])
    is_pdf = BooleanField('is_pdf', default='y', validators=[])

    # input fields for times
    time_1 = DateTimeField('time_1', format='%H:%M', default=datetime.time(8, 0), validators=[w_val.DataRequired()])
    time_2 = DateTimeField('time_2', format='%H:%M', validators=[w_val.Optional()])
    time_3 = DateTimeField('time_3', format='%H:%M', validators=[w_val.Optional()])
    time_4 = DateTimeField('time_4', format='%H:%M', validators=[w_val.Optional()])
    time_5 = DateTimeField('time_5', format='%H:%M', validators=[w_val.Optional()])
    time_6 = DateTimeField('time_6', format='%H:%M', validators=[w_val.Optional()])

