from sun_database import SunDatabase
from obj_file_loader import ObjFile
import datetime
import matplotlib.pyplot as plt
from shading_calculator_functions.shading_polygon_calculators import adjacent_overlapping_shading_polygon, \
    find_obj_base_polygons, shadow_polygons_at_multiple_times
from shapely_operations.shapely_plotter import shapely_plot
from shapely_operations.add_shapely_to_obj_file import add_shapely_to_obj_file
from shading_calculator_functions.vertex_operations import simplify_vertices
from matplotlib.pyplot import Axes
import time
from typing import List
from shading_logger import shading_logger


def shading_calculator_for_model(obj_file_path: str, sun_database_path: str, sun_database_sheetname: str,
                                 matplotlib_axes: Axes,
                                 times: List[datetime.time],
                                 north_angle: float,
                                 original_base_plane: str,
                                 x_resolution: float, y_resolution: float, z_resolution: float,
                                 base_plane_height: float,
                                 base_plane_tolerance: float,
                                 number_of_bases: int,
                                 start_alpha: float,
                                 alpha_increment: float,
                                 alpha_timeout_interval: float,
                                 aim_cover_percentage: float,
                                 simplification_tolerance: float,
                                 export_path: str,
                                 export_filename: str,
                                 do_display: bool=False):

    """
    The function takes in a .obj file as input and based on a sun database - imported from an excel file - it calculates
    the shadow polygons on the object based on the given times. The result is stored in a new.obj file and in .pdf
    format.
    :param obj_file_path: the file path to the.obj file
    :param sun_database_path: the file path to the sun database excel file
    :param sun_database_sheetname: the sheetname of the sun database in the excel file
    :param matplotlib_axes: a maptlotlip.pyplot Axes instance -> canvas for the .pdf plotting
    :param times: the times when the shadow polygons are needed to be calculated. Shall be given in datetime format.
    :param north_angle: The angle of the direction oof north. It is the y axis (0,1,0) vector by default.
        y
        ^     north angle
        |    /
        |  /
        |/________> x
    :param original_base_plane: the base plane of the model of the input .obj file. Must be "xy", "xz" or "yz".
    :param x_resolution: a float number, the interval size where the vertices are merged aligned to the x axis.
    :param y_resolution: a float number, the interval size where the vertices are merged aligned to the y axis.
    :param z_resolution: a float number, the interval size where the vertices are merged aligned to the z axis.
    :param base_plane_height: a float number, specifies the height of the plane where the foundation of the model is
    considered to be and also the shadows are cast on this plane.
    :param base_plane_tolerance: a float number, specifies a height from the base plane, needed for the calculation of
    the base polygon geometry. All vertices that are inside the base plane tolerance (have higher Z values than the
    base plane but lower than base plane + tolerance) are taken into account when calculating the base polygon of the
    object.
    :param number_of_bases: an integer, the number of expected base polygons - number of object in the .obj file.
    :param start_alpha: a float number, the start value of the Alpha shape algorithm when searching for point cloud
    boundaries.
    :param alpha_increment: a float number, the value of incrementation of the alpha value in the Alpha shape algorithm.
    :param alpha_timeout_interval: the time interval of timeout of the the alpha_shape_vertices function. If cover
    percentage of the alpha shape does not increase for a time specified in this interval, the function will exit and
    return the alpha shape with the highest cover percentage.
    :param aim_cover_percentage: a float value between 0 and 1. Specified the target cover percentage of the boundary
    polygon of a point cloud. The cover percentage counts vertices that are WITHIN the polygon! This means that only
    those vertices that are in the inside of the polygon, not only intersecting its boundary.
    :param simplification_tolerance: a float number, the input parameters of the shapely polygon simplification method.
    The higher the value is, the less sides the polygon will have, but it will also lose accuracy.
    :param export_path: the path to the export folder.
    :param export_filename: the name of the export file. Both .pdf and .obj will be saved with this filename.
    :param do_display: decides whether the matplotlib window shall be displayed or not.
    :return: an ObjFile instance and the exports to the export folder.
    """

    shading_logger.info("\nSTART OF SHADING CALCULATION FOR {0}".format(obj_file_path))
    shading_logger.info("SUN DATABASE USED: {0}".format(sun_database_path))
    # initializing input data class instances
    obj_type_ignore_list = ["vn", "f", "vt", "#"]
    obj_file = ObjFile().load_from_file(obj_file_path, obj_type_ignore_list,
                                        input_base_plane=original_base_plane)

    shading_logger.info("\t.obj file initialized, ignored types: {0}".format(obj_type_ignore_list))

    sd = SunDatabase.from_excel(sun_database_path, sun_database_sheetname)
    shading_logger.debug("\tOriginal vertex number: {0}".format(len(obj_file.vertices)))
    tm = time.time()

    simpl_vertices = list(simplify_vertices([v for v in obj_file.vertices if v.z >= base_plane_height],
                                            ["x", "y", "z"],
                                            x_resolution, y_resolution, z_resolution))

    shading_logger.info("\tSimplification of vertex cloud done in: {0}"
                        " Number of vertices: {1}".format(time.time() - tm, len(simpl_vertices)))

    # vertices plotting for debug purposes
    matplotlib_axes.scatter([v.x for v in simpl_vertices], [v.y for v in simpl_vertices], marker=".")
    if do_display:
        plt.pause(interval=.2)

    # create base polygon of obj
    base_polys = find_obj_base_polygons(simpl_vertices, base_plane_height,
                                        base_plane_tolerance=base_plane_tolerance,
                                        start_alpha=start_alpha,
                                        alpha_increment=alpha_increment,
                                        aim_cover_percentage=aim_cover_percentage,
                                        simplification_tolerance=simplification_tolerance,
                                        num_bases=number_of_bases,
                                        alpha_timeout_interval=alpha_timeout_interval)

    shading_logger.info("\tFinding base polygon for model is done.")

    # base poly plot for debug purposes
    shapely_plot(base_polys, matplotlib_axes, color="g", linewidth=.75)
    if do_display:
        plt.pause(interval=.2)
    # plt.waitforbuttonpress()

    # create shadow polygons at desired times
    shadow_polys = shadow_polygons_at_multiple_times(times, simpl_vertices, base_polys, sd,
                                                     north_angle=north_angle,
                                                     base_plane_height=base_plane_height,
                                                     start_alpha=start_alpha,
                                                     alpha_increment=alpha_increment,
                                                     alpha_timeout_interval=alpha_timeout_interval,
                                                     aim_cover_percentage=aim_cover_percentage,
                                                     simplification_tolerance=simplification_tolerance)

    shading_logger.info("\tCalculation of shading polygons is done.")
    # base poly plot for debug purposes

    shapely_plot(shadow_polys, matplotlib_axes, color="g", linewidth=.75)
    if do_display:
        plt.pause(interval=.2)

    # create merged polygons on adjacent times
    ov_poly = adjacent_overlapping_shading_polygon(shadow_polys, base_polys)

    shading_logger.info("\tCalculation of intersection areas of shading polygons is done.")

    # plot merged polygon to visualizer interface
    shapely_plot(ov_poly, matplotlib_axes, color="m", linewidth=.8)
    if do_display:
        plt.pause(interval=.2)

    # add merged polygons to the original ObjFile instance
    # add_shapely_to_obj_file(ov_poly, obj_file)

    # dump ObjFile class instance to file
    new_obj = ObjFile()
    add_shapely_to_obj_file(base_polys, new_obj, as_group=True)
    add_shapely_to_obj_file(shadow_polys, new_obj, as_group=True)
    add_shapely_to_obj_file(ov_poly, new_obj, as_group=True)
    new_obj.dump_to_file(export_path, export_filename + ".obj")
    shading_logger.info("\tResults are dumped to the {0} file".format(export_path + export_filename + ".obj"))
    # save visualizer plot into file
    plt.savefig(export_path + export_filename + ".pdf", format="pdf")

    shading_logger.info("\tOutput documents are saved to the {0} file".format(export_path + export_filename + ".pdf"))
    shading_logger.info("END OF SHADING CALCULATION FOR {0}\n".format(obj_file_path))
    if do_display:
        plt.pause(interval=.2)

    return new_obj
