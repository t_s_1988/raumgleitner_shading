from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon
from mpl_toolkits.mplot3d import Axes3D


def point_plot(input_point: Point, to_axes, **kwargs):
    if isinstance(to_axes, Axes3D):
        to_axes.scatter(input_point.x,
                        input_point.y,
                        input_point.z if input_point.has_z else 0., **kwargs)
    else:
        to_axes.scatter(input_point.x,
                        input_point.y, **kwargs)


def polygon_plot(input_polygon: Polygon, to_axes, **kwargs):
    if isinstance(to_axes, Axes3D):
        to_axes.plot([c[0] for c in input_polygon.boundary.coords],
                     [c[1] for c in input_polygon.boundary.coords],
                     [c[2] if input_polygon.has_z else 0 for c in input_polygon.boundary.coords], **kwargs)
    else:
        to_axes.plot([c[0] for c in input_polygon.boundary.coords],
                     [c[1] for c in input_polygon.boundary.coords], **kwargs)


def multipoint_plot(input_mp: MultiPoint, to_axes, **kwargs):
    for p in input_mp.geoms:
        point_plot(p, to_axes, **kwargs)


def multipolygon_plot(input_mpl: MultiPolygon, to_axes, **kwargs):
    for pl in input_mpl.geoms:
        polygon_plot(pl, to_axes, **kwargs)
