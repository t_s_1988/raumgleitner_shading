from shapely.geometry import Point, Polygon, MultiPolygon
from obj_file_loader.obj_type_classes import Vertex, Face
from obj_file_loader import ObjFile, ObjGroup


def point_to_vertex(input_point: Point) -> Vertex:
    return Vertex(input_point.x, input_point.y, input_point.z)


def polygon_to_face(input_poly: Polygon) -> Face:
    face = Face()
    face.vertices = [Vertex(c[0], c[1], c[2]) for c in input_poly.boundary.coords]
    return face


def point_to_obj_file(input_point: Point, obj_file: ObjFile, as_group: bool) -> ObjFile:
    vtx = point_to_vertex(input_point)
    obj_file.vertices.append(vtx)
    if as_group:
        new_group = ObjGroup(input_point.__str__().replace(" ", "-"), obj_file)
        new_group.vertices.append(vtx)
    return obj_file


def polygon_to_obj_file(input_poly: Polygon, obj_file: ObjFile, as_group: bool) -> ObjFile:
    face = polygon_to_face(input_poly)
    obj_file.vertices.extend(face.vertices)
    obj_file.texture_vertices.extend(face.texture_vertices)
    obj_file.vertex_normals.extend(face.vertex_normals)
    obj_file.faces.append(face)
    if as_group:
        new_group = ObjGroup(input_poly.__str__().replace(" ", "-"), obj_file)
        new_group.vertices.extend(face.vertices)
        new_group.texture_vertices.extend(face.texture_vertices)
        new_group.vertex_normals.extend(face.vertex_normals)
        new_group.faces.append(face)
    return obj_file


def multipolygon_to_obj_file(input_mp: MultiPolygon, obj_file: ObjFile, as_group: bool) -> ObjFile:
    for pl in input_mp.geoms:
        polygon_to_obj_file(pl, obj_file, as_group)
    return obj_file
