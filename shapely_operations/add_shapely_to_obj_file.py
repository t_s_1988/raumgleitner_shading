from shapely_operations import _shapely_elements_to_obj_format as sh_to_obj
from shapely.geometry.base import BaseGeometry
from obj_file_loader import ObjFile


def add_shapely_to_obj_file(input_geom: BaseGeometry, obj_file: ObjFile, as_group: bool):

    instance_class_name = input_geom.__class__.__name__.lower()

    try:
        curr_dump_function = getattr(sh_to_obj, "{0}_to_obj_file".format(instance_class_name))
        curr_dump_function(input_geom, obj_file, as_group)
    except AttributeError:
        raise NotImplementedError("{0} addition to .obj file is not yet implemented!".format(instance_class_name))
