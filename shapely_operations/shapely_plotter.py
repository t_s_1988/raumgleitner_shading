from shapely_operations import _shapely_elements_plot_to_matplotlib as sh_mp
from shapely.geometry.base import BaseGeometry


def shapely_plot(input_geom: BaseGeometry, to_axes, **kwargs):

    if not input_geom.is_empty:
        instance_class_name = input_geom.__class__.__name__.lower()

        try:
            curr_plot_function = getattr(sh_mp, "{0}_plot".format(instance_class_name))
            curr_plot_function(input_geom, to_axes, **kwargs)
        except AttributeError:
            raise NotImplementedError("{0} plotting is not yet implemented!".format(instance_class_name))
    else:
        print("Input geometry was empty, nothing was plotted.")
